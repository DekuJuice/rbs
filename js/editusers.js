function jsSetUserActive(userId, active)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=setUserActive&userId="+userId+"&active="+active);
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            updateUserList();
        }
    };
    return false;
}

function jsSetUserAdmin(userId, isAdmin)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=setUserAdmin&userId="+userId+"&isAdmin="+isAdmin);
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            updateUserList();
        }
    };
    return false;
}

function updateUserList()
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=displayUserList");
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById("userList").innerHTML = this.responseText;
        }
    };
}