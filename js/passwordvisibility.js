
function togglePasswordVisibility()
{
    var password = document.getElementsByClassName("passInput");
    for (var i = 0; i < password.length; i++)
    {
        var input = password[i];
        if (input.type === "password")
            input.type = "text";
        else
            input.type = "password";
    }
}