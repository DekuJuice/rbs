function jsAddDay()
{
    var dayName = document.getElementById("dayNameInput").value;
    
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=addNewDay&dayName="+dayName);
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById("dayNameInput").value = "";
            jsUpdateDayList();
        }
    };
    
    return false;
}

function jsDeleteDay(id)
{
    if (confirm("Are you sure you want to delete this day? This action cannot be undone."))
    {
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "php/async_request.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("action=deleteDay&dayId="+id);
        
        xhttp.onreadystatechange=function() {
            if (this.readyState == 4 && this.status == 200)
            {
                jsUpdateDayList();
            }
        };
    }
    
    return false;
}

function jsAddBlock()
{
    var blockName = document.getElementById("blockNameInput").value;
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=addNewBlock&blockName="+blockName);
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById("blockNameInput").value = "";
            jsUpdateBlockList();
            jsUpdateDayList();
        }
    };
    
    return false;
}

function jsDeleteBlock(id)
{
    if (confirm("Are you sure you want to delete this block? This action cannot be undone."))
    {
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "php/async_request.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("action=deleteBlock&blockId="+id);
        
        xhttp.onreadystatechange=function() {
            if (this.readyState == 4 && this.status == 200)
            {
                jsUpdateBlockList();
                jsUpdateDayList();
            }
        };
    }
    
    return false;
}

function jsSetBlockTime(form)
{

    var dayId = form.getElementsByClassName("dayId")[0].value;
    var blockId = form.getElementsByClassName("blockId")[0].value;
    var timeStart = form.getElementsByClassName("timeStart")[0].value;
    var timeEnd = form.getElementsByClassName("timeEnd")[0].value;
    
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=setBlockTime&dayId="+dayId
    +"&blockId="+blockId
    +"&timeStart="+timeStart
    +"&timeEnd="+timeEnd
    );
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            //lol
            var tbody = form.parentNode.parentNode.parentNode;
            jsUpdateBlockTimes(dayId, tbody);        
        }
    };
    
    return false;
}

function jsAddSchedule()
{
    var startDate = document.getElementById("scheduleStartInput").value;
    var endDate = document.getElementById("scheduleEndInput").value;
    
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=addNewSchedule&startDate="+startDate+"&endDate="+endDate);
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById("scheduleStartInput").value = "";
            document.getElementById("scheduleEndInput").value = "";
            
            jsUpdateScheduleList();
            
        }
    };
    
    return false;
}

function jsDeleteSchedule(id)
{
    if (confirm("Are you sure you want to delete this schedule? This action cannot be undone."))
    {
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "php/async_request.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("action=deleteSchedule&scheduleId="+id);
        
        xhttp.onreadystatechange=function() {
            if (this.readyState == 4 && this.status == 200)
            {
                jsUpdateScheduleList();
            }
        };
    }
    
    return false;
}

function jsSetDayOrder(scheduleId, positionInOrder, nextPosition, form)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=setDayOrder&scheduleId="+scheduleId
        +"&oldDayPosition="+positionInOrder
        +"&newDayPosition="+nextPosition
    );
    
    var tbody = form.parentNode.parentNode.parentNode.parentNode;
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            jsUpdateScheduleDays(scheduleId, tbody);
        }
    };
}

function jsAddScheduleDay(scheduleId, form)
{
    var newDayId = form.getElementsByClassName("scheduleDayId")[0].value;
    var tbody = form.parentNode.getElementsByTagName("table")[0];

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=addScheduleDay&scheduleId="+scheduleId+"&dayId="+newDayId);
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            jsUpdateScheduleDays(scheduleId, tbody);
        }
    };
    
    return false;
}

function jsDeleteScheduleDay(scheduleId, positionInOrder, form)
{
    var xhttp = new XMLHttpRequest();
    var tbody = form.parentNode.parentNode.parentNode.parentNode;
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=deleteScheduleDay&scheduleId="+scheduleId+"&positionInOrder="+positionInOrder);
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            jsUpdateScheduleDays(scheduleId, tbody);

        }
    };
    
    return false;
}

function jsAddException(form)
{

    var exceptionDate = document.getElementById("exceptionDateInput").value;
    var exceptionType = document.getElementById("exceptionTypeInput").value;
    var replacementDay = document.getElementById("exceptionReplacementInput").value;

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");   
    xhttp.send("action=addException"
        +"&exceptionDate="+exceptionDate
        +"&exceptionType="+exceptionType
        +"&replacementDay="+replacementDay
    );
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            jsUpdateScheduleExceptions();
        }
    };
    
    return false;
}

function jsDeleteException(id)
{
    if (confirm("Are you sure you want to delete this exception? This action cannot be undone."))
    {
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "php/async_request.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("action=deleteException&exceptionId="+id);
        
        xhttp.onreadystatechange=function() {
            if (this.readyState == 4 && this.status == 200)
            {
                jsUpdateScheduleExceptions();
            }
        };
    }
    
    return false;
}

function jsUpdateDayList()
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=displayDayList");
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById("dayList").innerHTML = this.responseText;
        }
    };
}

function jsUpdateBlockTimes(dayId, tbody)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=displayBlockTimes&dayId="+dayId);
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            tbody.innerHTML = this.responseText;
        }
    };
}

function jsUpdateBlockList()
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=displayBlockList");
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById("blockList").innerHTML = this.responseText;
        }
    };
}

function jsUpdateScheduleList()
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=displayScheduleList");
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById("scheduleList").innerHTML = this.responseText;
        }
    };
}

function jsUpdateScheduleDays(scheduleId, tbody)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=displayScheduleDays&scheduleId="+scheduleId);
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            tbody.innerHTML = this.responseText;
        }
    };
}

function jsUpdateScheduleExceptions()
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=displayExceptionList");
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById("exceptionList").innerHTML = this.responseText;
        }
    };
}

function jsSetDayName(id, form)
{
    var textField = form.getElementsByClassName("dayNameInput")[0];
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=setDayName&dayId="+id+"&dayName="+textField.value);
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            jsUpdateDayList();
        }
    };  
    
    return false;
}

function jsSetBlockName(id, form)
{
    var textField = form.getElementsByClassName("blockNameInput")[0];
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=setBlockName&blockId="+id+"&blockName="+textField.value);
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            jsUpdateBlockList();
            jsUpdateScheduleList();
            jsUpdateDayList();
        }
    };  
    
    return false;
}

function jsUpdateDayPreview(input)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=displayDayName&date="+input.value);
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById("exceptionDayPreview").innerHTML = this.responseText;
        }
    };  
}

function hideNextRow(element, event)
{
    var next = element.nextElementSibling;
    if (next)
        next.hidden = !next.hidden;
}

function showTab(id)
{
    var tabs = document.getElementsByClassName("tabContent");
    for (var i = 0; i < tabs.length; i++)
    {
        tabs[i].hidden = true;
    }
    
    var target = document.getElementById(id);
    target.hidden = false;
}

function setTextFieldDirty(field)
{
    if (!field.classList.contains("unsavedInput") && field.value !== field.defaultValue)
        field.classList.add("unsavedInput");
    else if (field.value === field.defaultValue)
        field.classList.remove("unsavedInput");
}