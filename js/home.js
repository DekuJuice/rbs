function displayAllBookings()
{
    var date = document.getElementById("allBookingsDate").value;
    var room = document.getElementById("allBookingsRoom").value;
    
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=displayAllBookings&date="+date+"&room="+room);
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById("allBookingsList").innerHTML = this.responseText;
        }
    };
}

function jsDisplayDayBlocks(form)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=displayDayBlocks&date="+form.value);
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById("blockPreview").innerHTML = this.responseText;
        }
    };
}