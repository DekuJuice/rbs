
function jsAddNewRoom()
{

    var roomName = document.getElementById("roomNameInput").value;
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=addNewRoom&roomName="+roomName);
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            jsUpdateRoomControlPanel();
        }
    };    
    
    return false;
}

function jsDeleteRoom(id)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=deleteRoom&roomId="+id);
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            jsUpdateRoomControlPanel();
        }
    };  
    
    return false;
}

function jsSetRoomActive(id, form)
{
    var targetState = form.getElementsByClassName("roomActiveValue")[0].value;
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=setRoomActive&roomId="+id+"&roomIsActive="+targetState);
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            jsUpdateRoomControlPanel();
        }
    };  
    
    return false;
}

function jsSetRoomName(id, form)
{
    var textField = form.getElementsByClassName("roomNameInput")[0];
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=setRoomName&roomId="+id+"&roomName="+textField.value);
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            jsUpdateRoomControlPanel();
        }
    };  
    
    return false;
}

function jsUpdateRoomControlPanel()
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=displayRoomList");
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById("roomControlPanel").innerHTML = this.responseText;
        }
    };    
}

function setTextFieldDirty(field)
{
    if (!field.classList.contains("unsavedInput") && field.value !== field.defaultValue)
        field.classList.add("unsavedInput");
    else if (field.value === field.defaultValue)
        field.classList.remove("unsavedInput");
}