function jsCancelBooking(id)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=cancelBooking&bookingId="+id);
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            updateBookingList();
            
        }
    };
    return false;
}

function jsMakeBooking(roomId, blockId, date, element)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=makeBooking&roomId="+roomId+"&blockId="+blockId+"&bookingDate="+date);
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            updateBookingList();
            element.parentNode.innerHTML = "Booked!";
        }
    };
    
    return false;
    
}

function updateBookingList()
{   
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=displayUserBookings");
    
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById("bookingList").innerHTML = this.responseText;
        }
    };    
}

function updateSearchResults()
{
    //Determine which rooms to search for

    var postData = {};
    var rooms = [];
    var checkboxes = document.getElementsByClassName("roomSelector");
    for (var i = 0; i < checkboxes.length; i++)
    {
        if (checkboxes[i].checked)
        {
            rooms.push(checkboxes[i].value);
        }
    }
        
    postData.rooms = rooms;
        
    //Determine which blocks to search for
    
    var blocks = [];
    var checkboxes = document.getElementsByClassName("blockSelector");
    for (var i = 0; i < checkboxes.length; i++)
    {
        if (checkboxes[i].checked)
        {
            blocks.push(checkboxes[i].value);
        }
    }
        
    postData.blocks = blocks;
    postData.dateStart = document.getElementById("dateStart").value;
    postData.dateEnd = document.getElementById("dateEnd").value;
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange=function() {
        if (this.readyState == 4 && this.status == 200)
        {
            if (this.responseText === "")
                document.getElementById("searchResults").innerHTML = "<p>No Results Found.</p>";
            else
                document.getElementById("searchResults").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", "php/async_request.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=searchBookings&param="+JSON.stringify(postData));
    
    return false;
}

function hideNextRow(element, event)
{
    var next = element.nextElementSibling;
    if (next)
        next.hidden = !next.hidden;
}

function showTab(id)
{
    var tabs = document.getElementsByClassName("tabContent");
    for (var i = 0; i < tabs.length; i++)
    {
        tabs[i].hidden = true;
    }
    
    var target = document.getElementById(id);
    target.hidden = false;
}