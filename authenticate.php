<?php
    require_once("private/registration.php");
    
    // Failed to authenticate
    if (!(isset($_GET["authenticationToken"]) && authenticateUser($_GET["authenticationToken"])) )
    {
        header("Location: index.php");
        exit;
    }

?>

<!doctype HTML>
<html lang = "en">
    
    <head>
        <title>Authentication</title>
        <link rel = "stylesheet" type = "text/css" href= "style/global.css">        
        <meta charset = "utf-8">
    </head>

    <body>           
        <p>Your account has been successfully authenticated. You may now login.</p>
        <a href="index.php">Return to the Home Page</a>
    </body>

</html>