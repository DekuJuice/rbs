<?php
    session_start();
    require_once("private/utility.php");
    require_once("private/registration.php");
    
    $config = parse_ini_file("private/config.ini");
    $registrationError = null;

    if (isset($_POST["register"]))
    {
        $registrationError = registerUser(
            $_POST["newEmail"],
            $_POST["newRealName"],
            $_POST["newPassword"],
            $_POST["confirmPassword"]
        );
        
        savePostValue("newEmail");
        savePostValue("newRealName");
        
        // Registered successfully
        if (count($registrationError) === 0)
        {
            header("Location: registration_success.html");
            exit;
        }
    }
    
?>

<!doctype HTML>
<html lang = "en">
    <head>
        <title>Register</title>
        <link rel = "stylesheet" type = "text/css" href= "style/responsivegrid/col.css">
        <link rel = "stylesheet" type = "text/css" href= "style/responsivegrid/3cols.css">
        <link rel = "stylesheet" type = "text/css" href= "style/global.css">
        <link rel = "stylesheet" type = "text/css" href= "style/register.css">
        
        <meta charset = "utf-8">
    </head>

    <body>
        <div class="section group">
            
            <div class="col span_1_of_3"></div>
            <div class="col span_1_of_3">
        
                <a href="index.php">Back</a>
        
                <?php
                    if (isset($registrationError))
                    {
                        foreach ($registrationError as &$err)
                        {
                            echo "<p class='errorMessage'>$err</p>";
                        }
                    }
                
                ?>

                <div id="registrationContainer">
                    <form action = "registration.php" method = "post" >
                        Email:
                        <div class="iconContainer">
                            
                            <img src="resource/email.png" class="inputIcon">

                            <input 
                                type = "text" 
                                name = "newEmail"
                                class = "emailInput"
                                placeholder = "john.doe"
                                value = <?php displaySavedPostValue("newEmail"); ?>
                            > 
                            <span> <?php echo $config["emailDomain"] ?> </span>
                        </div>
                        
                        Real Name:
                        <div class="iconContainer">
                            <div class="inputIcon"></div>

                            <input 
                                type = "text" 
                                name = "newRealName"
                                placeholder = "John Doe"
                                value = <?php displaySavedPostValue("newRealName"); ?>
                            > <br>
                        </div>
                            
                        Password:
                        <div class="iconContainer">
                        
                            <img src="resource/password.png" class="inputIcon">

                            <input 
                                type = "password"
                                name = "newPassword" 
                                class="passInput"
                            >
                            <label><input type = "checkbox" onclick = "togglePasswordVisibility()">Show Passwords</label>

                        </div>
                        
                        Confirm Password:
                        <div class="iconContainer">
                            <img src="resource/password.png" class="inputIcon">
                            
                            <input
                                type = "password"
                                name = "confirmPassword"
                                class = "passInput"
                            >
                        </div>

                        <input type = "submit" value = "Submit" name="register">
                    </form>
                </div>
                
            </div>
            
            <div class="col span_1_of_3"></div>

        </div>

        <script src = "js/passwordvisibility.js"></script>

    </body>

</html>