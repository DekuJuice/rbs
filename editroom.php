<?php
    session_start();
    if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
    {
        header("Location: index.php");
        exit;
    }
    
    require_once("private/database.php");
    require_once("private/room_control_panel.php");
    $connection = db_connect();
    
    if (isset($_POST["addNewRoom"]))
    {
        addNewRoom($_POST["newRoomName"]);
    }
    elseif (isset($_POST["deleteRoom"]))
    {
        deleteRoom($_POST["roomId"]);
    }
    elseif (isset($_POST["setRoomName"]))
    {
        setRoomName($_POST["roomId"], $_POST["newRoomName"]);
    }
    elseif (isset($_POST["setRoomActive"]))
    {
        setRoomActive($_POST["roomId"], $_POST["roomIsActive"]);
    }
?>

<!doctype HTML>
<html lang = "en">
    
    <head>
        <title>Edit Rooms</title>
        <link rel = "stylesheet" type = "text/css" href= "style/responsivegrid/col.css">
        <link rel = "stylesheet" type = "text/css" href= "style/responsivegrid/4cols.css">
        <link rel="stylesheet" type="text/css" href="style/global.css">
        <link rel="stylesheet" type="text/css" href="style/editroom.css">

        <meta charset = "utf-8">
    </head>

    <body>
        <div class="section group">
            <div class = " col span_4_of_4">
                <a href = "php/logout.php">Log Out</a>    
                <a href="home.php">Back</a>
            </div>
        </div>
        <div class = "section group">
            <div class = "col span_1_of_4"> </div>
            <div class = "col span_2_of_4">
                
                <h2>Rooms</h2>

                <form action = "editroom.php" method="post" onsubmit="return jsAddNewRoom();">
                    <input type="text" placeholder="Room Name" name="newRoomName" id="roomNameInput" >
                    <input type="submit" value="Add new room" name="addNewRoom">
                </form>        

                <table id = "roomList">
                    <thead>
                        <tr>
                            <th>Actions</th>
                            <th>ID</th>
                            <th>Room Name</th>
                        </tr>
                    </thead>
                    
                    <tbody id = "roomControlPanel">
                    <?php
                        displayRoomList();
                    ?>
                    </tbody>
                        
                </table>
            </div>
            <div class = "col span_1_of_4"> </div>

        </div>

        <script src="js/editroom.js"></script>
                
    </body>

</html>

<?php
    db_close();
?>