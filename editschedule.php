<?php
    session_start();
    if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
    {
        header("Location: index.php");
        exit;
    }
    
    require_once("private/database.php");
    require_once("private/schedule_control_panel.php");
    require_once("private/schedule.php");

    $connection = db_connect();
    
    if (isset($_POST["addNewDay"]))
    {
        addNewDay($_POST["newDayName"]);
    }
    elseif (isset($_POST["deleteDay"]))
    {
        deleteDay($_POST["dayId"]);
    }
    elseif (isset($_POST["setBlockTime"]))
    {
        setBlockTime($_POST["dayId"], $_POST["blockId"], $_POST["timeStart"], $_POST["timeEnd"]);
    }
    elseif (isset($_POST["addNewBlock"]))
    {
        addNewBlock($_POST["newBlockName"]);
    }
    elseif (isset($_POST["deleteBlock"]))
    {
        deleteBlock($_POST["blockId"]);
    }
    elseif (isset($_POST["addSchedule"]))
    {
        addSchedule($_POST["scheduleStartDate"], $_POST["scheduleEndDate"]);
    }
    elseif (isset($_POST["deleteSchedule"]))
    {
        deleteSchedule($_POST["scheduleId"]);
    }
    elseif (isset($_POST["addException"]))
    {
        addException($_POST["exceptionDate"], $_POST["exceptionType"], $_POST["replacementDay"]);
    }
    elseif (isset($_POST["deleteException"]))
    {
        deleteException($_POST["exceptionId"]);
    }
    elseif (isset($_POST["moveDayUp"]))
    {
        setDayOrder($_POST["scheduleId"], $_POST["dayPosition"], $_POST["dayPosition"] - 1);
    }
    elseif (isset($_POST["moveDayDown"]))
    {
        setDayOrder($_POST["scheduleId"], $_POST["dayPosition"], $_POST["dayPosition"] + 1);
    }
    elseif (isset($_POST["addScheduleDay"]))
    {
        addScheduleDay($_POST["scheduleId"], $_POST["scheduleDayId"]);
    }
    elseif (isset($_POST["deleteScheduleDay"]))
    {
        deleteScheduleDay($_POST["scheduleId"], $_POST["dayPosition"]);
    }
?>

<!doctype HTML>
<html lang="en">
    <head>
        <title>Edit Schedule</title>

        <link rel = "stylesheet" type = "text/css" href= "style/responsivegrid/col.css">
        <link rel = "stylesheet" type = "text/css" href= "style/responsivegrid/8cols.css">
        <link rel = "stylesheet" type = "text/css" href= "style/global.css">
        <link rel = "stylesheet" type = "text/css" href= "style/editschedule.css">
        <meta charset= "utf-8">
    </head>
    
    <body>
        <div class = "section group">
            <div class = "col span_8_of_8">
                <nav>
                    <a href = "php/logout.php">Log Out</a>    
                    <a href="home.php">Back</a>
                </nav>
            </div>
        </div>
        
        <div class = "section group">
            
            <div class = "col span_1_of_8"></div>
            <div class = "col span_3_of_8">
                <h3>Days</h3>
                <form action="editschedule.php" method="post" onsubmit="return jsAddDay();">
                    <input type="text" name="newDayName" placeholder="Day Name" id="dayNameInput">
                    <input type="submit" value="Add day" name="addNewDay">
                </form>
                
                <table>
                    <thead>
                        <tr>
                            <th>Actions </th>
                            <th>Day ID</th>
                            <th>Day Name</th>
                        </tr>
                    </thead>
                    
                    <tbody id="dayList">
                        <?php
                            displayDayList();  
                        ?>
                    </tbody>           
                </table>
            </div>
            
            <div class = "col span_3_of_8">
                <h3>Blocks</h3>
                <form action="editschedule.php" method="POST" onsubmit="return jsAddBlock();">
                    <input type="text" name="newBlockName" placeholder="Block Name" id="blockNameInput">
                    <input type="submit" value="Add block" name="addNewBlock">
                </form>

                <table>
                    <thead>
                        <tr>
                            <th>Actions</th>
                            <th>Block ID</th>
                            <th>Block Name</th>
                        </tr>
                    </thead>
                    <tbody id="blockList">
                        <?php
                            displayBlockList();
                        ?>
                    </tbody>
            
                </table>
            </div>
            
            <div class = "col span_1_of_8"></div>
        </div>
        
        <div class = "section group">
            <div class = "col span_1_of_8"></div>
            <div class = "col span_3_of_8">
                <h3>Schedules</h3>
                <form action="editschedule.php" method="post" onsubmit ="return jsAddSchedule();" >
                    <input type="date" name="scheduleStartDate" id="scheduleStartInput"> Start Date<br>
                    <input type="date" name="scheduleEndDate" id="scheduleEndInput"> End Date<br>
                    <input type="submit" value="Add Schedule" name="addSchedule">
                </form>

                <table>
                    <thead>
                        <tr>
                            <th>Actions</th>
                            <th>ID</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                        </tr>
                    </thead>
                        
                    <tbody id="scheduleList">
                    <?php
                        displayScheduleList();
                    ?>
                    </tbody>
                        

                </table>
            </div>

            <div class = "col span_3_of_8">
                <h3>Exceptions</h3>
                <form action="php/addexception.php" method="post" onsubmit="return jsAddException();">
                    Date <input type="date" name="exceptionDate" id="exceptionDateInput" onchange="jsUpdateDayPreview(this);"
                        <?php
                        $today = date("Y-m-d");
                        echo "value='$today'";  
                        ?>
                    >
                    
                    <span id="exceptionDayPreview">
                        <?php
                            displayDayName($today);
                        ?>
                    </span>
                    
                    <br>
                    Exception Type <select name="exceptionType" id="exceptionTypeInput">
                        <?php
                            $stmt = mysqli_prepare($connection, "
                                SELECT id, exceptionName FROM schedule_exception_types
                                ORDER BY id
                            ");
                            
                            $rc = mysqli_stmt_bind_result($stmt, $exceptionId, $exceptionName);
                            $rc = mysqli_stmt_execute($stmt);
                            while (mysqli_stmt_fetch($stmt))
                            {
                                echo "<option value='$exceptionId'>$exceptionName</option>";
                            }
                            $rc = mysqli_stmt_close($stmt);
                        ?>
                    </select> <br>
                    Replacement Day <select name="replacementDay" id="exceptionReplacementInput">
                        <?php
                            $stmt = mysqli_prepare($connection, "
                                SELECT id, dayName FROM days
                                ORDER BY id
                            ");
                            
                            $rc = mysqli_stmt_bind_result($stmt, $dayId, $dayName);
                            $rc = mysqli_stmt_execute($stmt);
                            
                            while (mysqli_stmt_fetch($stmt))
                            {
                                echo "<option value='$dayId'>$dayName</option>";
                            }
                            $rc = mysqli_stmt_close($stmt);
                            
                        ?>
                        
                    </select> <br>

                    <input type="submit" value="Add Exception" name="addException">
                </form>
                    

                <table>
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Replacement Day</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                        
                    <tbody id="exceptionList">
                        <?php
                            displayExceptionList();
                        ?>
                    </tbody>
                        
                        
                </table>
            </div>
            <div class="col span_1_of_8"></div>
        </div>
        
        <script src="js/editschedule.js"></script>
    </body>
</html>