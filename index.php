<?php
    session_start();
    
    if (isset($_SESSION["userCredentials"]))
    {
        header("Location: home.php");
        exit;
    }
    
    $config = parse_ini_file("private/config.ini");
    
    require_once("private/login.php");
    $loginError = null;
    
    if (isset($_POST["login"]))
    {
        $loginError = login($_POST["loginEmail"], $_POST["loginPassword"]);
        if (count($loginError) === 0)
        {
            header("Location: home.php");
            exit;
        }
    }
    
?>

<!doctype HTML>
<html lang = "en">
    
    <head>
        <title>RBS Login</title>
        <link rel = "stylesheet" type = "text/css" href= "style/responsivegrid/col.css">
        <link rel = "stylesheet" type = "text/css" href= "style/responsivegrid/3cols.css">
        <link rel = "stylesheet" type = "text/css" href= "style/login.css">
        <link rel = "stylesheet" type = "text/css" href= "style/global.css">
        <meta charset = "utf-8">
    </head>

    <body> 
        <div class="section group">
            <div class="col span_1_of_3"></div>
            <div class="col span_1_of_3">
            
                <div id="loginContainer">
                
                    <form action = "index.php" method="post" class = "loginForm">
                        
                        <img src= "resource/login.png" alt= "Avatar" id="loginAvatar">
                        
                        <div id="loginInfo">
                            <p>Welcome to the room booking system.</p>
                            <p>Please log in, <a href = "registration.php">or register a new account.</a> </p>
                        </div>
                        
                        <?php
                            if (isset($loginError))
                            {
                                foreach ($loginError as &$errorMessage)
                                {
                                    echo "<p class='errorMessage'>$errorMessage</p>";
                                }
                            }
                        ?>
                            
                        Email:
                        <div class="iconContainer">
                            <img src="resource/email.png" class="inputIcon">
                            <input type = "text" name = "loginEmail" class="emailInput" placeholder="john.doe">
                            <span><?php echo $config["emailDomain"] ?></span>
                        </div>
                        
                        Password:
                        
                        <div class="iconContainer">
                            <img src="resource/password.png" class="inputIcon">
                            <input type = "password" name = "loginPassword" class="passInput"> 
                            <label><input type = "checkbox" onclick = "togglePasswordVisibility()">Show Password<br></label>
                        </div>
                        
                        <input type = "submit" value = "Submit" id="submitButton" name="login">
                    </form>
                
                </div>
                
            </div>
            <div class="col span_1_of_3"></div>

        </div>
        
        <script src = "js/passwordvisibility.js"></script>
    </body>

</html>