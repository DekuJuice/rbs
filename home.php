<?php
    session_start();
    
    if (!isset($_SESSION["userCredentials"]))
    {
        header("Location: index.php");
        exit;
    }
    
    
    require_once("private/database.php");
    require_once("private/bookings.php");
    $connection = db_connect();
?>

<!doctype HTML>
<html lang = "en">
    
    <head>
        <title>Home</title>
        <link rel = "stylesheet" type = "text/css" href= "style/responsivegrid/col.css">
        <link rel = "stylesheet" type = "text/css" href= "style/responsivegrid/8cols.css">
        <link rel = "stylesheet" type = "text/css" href= "style/global.css">        
        <link rel = "stylesheet" type = "text/css" href= "style/home.css">        
        <meta charset = "utf-8">
    </head>

    <body> 
    
        <div class = "section group">
            <div class = "col span_8_of_8">
                <nav>
                    <a href = "php/logout.php">Log Out</a>
                    <a href = "manual.html">Manual</a>
                    <?php if ($_SESSION["userCredentials"]["isAdmin"]) : ?>
                    <a href = "editroom.php">Edit Rooms</a>
                    <a href = "editschedule.php">Edit Schedule</a>
                    <a href = "editusers.php">Edit Users</a>
                    <?php endif ?>
                </nav>                
            </div>

        </div>
            
        <div class = "section group">
            <div class = "col span_1_of_8"> </div>
            <div class = "col span_3_of_8">
                <div id="searchBookings">
                    <h2>Search For Bookings</h2>
                    <form action="index.php" method="POST" onsubmit = "return updateSearchResults();">
                        <div id="dateInputs">
                            <div>
                                Date Start:
                                <input type = "date" id="dateStart"
                                <?php
                                    $today = date("Y-m-d");
                                    echo "min='$today'";
                                        echo "value='$today'";  
                                ?>
                                > <br>
                                
                                Date End:
                                <input type = "date" id="dateEnd"
                                <?php
                                    echo "min='$today'";
                                    echo "value='$today'";  
                                ?>
                                >
                            </div>
                            
                            <div>
                                Block Preview
                                <input type = "date" id="blockPreviewDate"
                                <?php
                                    echo "min='$today'";
                                    echo "value='$today'";
                                ?>
                                onchange = "jsDisplayDayBlocks(this);" >
                                
                                <table>
                                    <tbody id="blockPreview">
                                        <?php
                                            displayDayBlocks($today);
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <div class="inputContainer">
                            
                            <div class="selectContainer">
                                <h3>Rooms to search</h3>
                                <div class="multiSelect" id="rooms">
                                    <?php
                                        $stmt = mysqli_prepare($connection, "
                                            SELECT friendlyName, id 
                                            FROM rooms
                                            WHERE active = 1
                                            ORDER BY friendlyName
                                        ");
                                                        
                                        $rc = mysqli_stmt_bind_result($stmt,$friendlyName, $roomId);
                                        $rc = mysqli_stmt_execute($stmt);
                                        while (mysqli_stmt_fetch($stmt))
                                        {
                                            echo "<label><input type='checkbox' class='roomSelector' value=$roomId >$friendlyName</label>";
                                        }
                                        $rc = mysqli_stmt_close($stmt);
                                        
                                    ?>
                                </div>
                            </div>
                        
                            <div class="selectContainer">
                                <h3>Time Blocks</h3>
                                <div class="multiSelect" id = "blocks">
                                    <?php
                                        $stmt = mysqli_prepare($connection, "
                                            SELECT blockName, id 
                                            FROM time_blocks
                                            ORDER BY id
                                        ");
                                            
                                        $rc = mysqli_stmt_bind_result($stmt, $blockName, $blockId);
                                        $rc = mysqli_stmt_execute($stmt);
                                            
                                        while (mysqli_stmt_fetch($stmt))
                                        {
                                            echo "<label><input type='checkbox' class='blockSelector' value=$blockId>$blockName</label>";
                                        }
                                    ?>
                                </div>
                            </div>
                            
                        </div>
                        <input type="submit" value="Search">
                    </form>
                                        
                </div>
            </div>
            
            <div class = "col span_3_of_8">
                <h2>Search Results</h2>
                <table>
                    <tbody id="searchResults">
                        <td>Press the search button to submit a query.</td>
                    </tbody>
                </table>

            </div>
            <div class="col span_1_of_8"></div>
        </div>
        
        <hr>
        
        <div class = "section group">
            <div class = "col span_1_of_8"> </div>
            <div class = "col span_3_of_8">

                <div>
                <h2>Your Bookings</h2>
                    <div class = "tableContainer">
                        <table id="bookingsTable">
                            <thead>
                                <tr>
                                    <th>Room</th>
                                    <th>Date</th>
                                    <th>Block</th>
                                    <th>Time Start</th>
                                    <th>Time End</th>
                                </tr>
                            </thead>
                            
                            <tbody id="bookingList">
                                <?php
                                    displayUserBookings();
                                ?>
                            </tbody>
                                        
                        </table>
                    </div>
                </div>
            
            </div>
            <div class="col span_3_of_8">
                <h2>Booking History</h2>
                Room:
                <select id="allBookingsRoom" onchange="displayAllBookings();">
                    <option value="allRooms">All Rooms</option>
                    <?php
                        $stmt = mysqli_prepare($connection, "
                            SELECT friendlyName, id 
                            FROM rooms
                            WHERE active = 1
                            ORDER BY friendlyName
                        ");
                                                
                        $rc = mysqli_stmt_bind_result($stmt,$friendlyName, $roomId);
                        $rc = mysqli_stmt_execute($stmt);
                        while (mysqli_stmt_fetch($stmt))
                        {
                            echo "<option value=$roomId>$friendlyName</option>";
                        }
                        $rc = mysqli_stmt_close($stmt);
                        
                    ?>
                </select> <br>
                
                Date:
                <input 
                    type="date" 
                    id = "allBookingsDate" 
                    value= <?php echo "'". date('Y-m-d')."'"; ?>
                    onchange="displayAllBookings();"
                >
                
                <div class="tableContainer">
                    <table>
                        <thead>
                            <tr>
                                <th>Booker</th>
                                <th>Room</th>
                                <th>Block</th>
                            </tr>
                        </thead>
                        <tbody id ="allBookingsList">
                            <?php
                                displayAllBookings("allRooms", date('Y-m-d'));
                            ?>
                        </tbody>
                    </table>
                </div>
            
            </div>
            <div class="col span_1_of_8"></div>

        </div>
        
        <script src = "js/bookings.js"></script>            
        <script src = "js/home.js"></script>
    </body>

</html>