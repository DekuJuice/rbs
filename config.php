
<!doctype HTML>
<html lang = "en">
    
    <head>
        <title>Setup</title>
        <meta charset = "utf-8">
    </head>

    <body>    

        <?php
            require_once("private/database.php");
            $connection = db_connect();
            mysqli_multi_query($connection, file_get_contents("private/table.sql") . file_get_contents("private/data.sql"));
            echo (mysqli_error($connection));

        ?>

  
        <p>it worked!</p>
    </body>
    
</html>
