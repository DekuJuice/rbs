<?php 

    require_once("database.php");
    require_once("schedule.php");
    
    function searchBookings($dateStart, $dateEnd, $roomFilter, $blockFilter)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]))
            return;
            
        $today = date("Y-m-d");
        // Startdate shouldn't be before today        
        //if ($dateStart < $today)
        //return;

        if ($dateEnd == null)
            return;


        // Enddate should be before start date            
        if ($dateStart > $dateEnd)
            return;
            
        $connection = db_connect();
        $days = getDayInfo($dateStart, $dateEnd);
        
        if ($days)
        {
            //Get every room in the system
            $rooms = [];
            $stmt = mysqli_prepare($connection, "
                SELECT id, friendlyName FROM rooms
            ");
            $rc = mysqli_stmt_bind_result($stmt, $roomId, $friendlyName);
            $rc = mysqli_stmt_execute($stmt);
            while (mysqli_stmt_fetch($stmt))
                $rooms[$roomId] = $friendlyName;

            $rc = mysqli_stmt_close($stmt);
            
            // Buffer the output to only display a table if search results are found
            ob_start();
            
            foreach ($days as &$dayInfo)
            {
                $day = $dayInfo['day'];
                            
                ob_start();
                
                //For each block that was specified to be checked
                foreach ($blockFilter as $blockId)
                {
                    // Make sure the current day has a time specified for this block
                    $block = $day["blocks"][$blockId];
                    
                    ob_start();
                    
                    if ($block && isset($block["timeStart"]) && isset($block["timeEnd"]))
                    {
                        $blockName = $block["name"];
                        $timeStart = $block["timeStart"];
                        $timeEnd = $block["timeEnd"];
                        
                        
                        // For each room that was specified to be checked
                        foreach ($roomFilter as $roomId)
                        {
                            // Make sure the room exists
                            if (isset($rooms[$roomId]))
                            {
                                // Check that there are no conflicting bookings
                                $stmt = mysqli_prepare($connection, "
                                        SELECT 
                                            1,
                                            users.realName,
                                            users.email
                                        FROM bookings
                                        LEFT JOIN users ON (users.id = bookings.userId)
                                        WHERE
                                            (bookings.bookingDate = ?)
                                            AND (bookings.roomId = ?)
                                            AND (bookings.blockId = ?)
                                    
                                ");
                                
                                $rc = mysqli_stmt_bind_param($stmt, "sii", $dayInfo['date'], $roomId, $blockId);
                                $rc = mysqli_stmt_bind_result($stmt, $exists, $userName, $userEmail);
                                $rc = mysqli_stmt_execute($stmt);
                                $rc = mysqli_stmt_fetch($stmt);
                                
                                $roomName = $rooms[$roomId];
                                    
                                if (!$exists)
                                {
                                    echo "<tr>
                                        <td>$roomName</td>
                                        <td><button onclick = 'jsMakeBooking($roomId, $blockId, \"{$dayInfo['date']}\", this)' >Book</button></td>
                                    </tr>";
                                }
                                else
                                {
                                    echo "<tr>
                                        <td>$roomName</td>
                                        <td>Booked by $userName ($userEmail)</td>
                                    </tr>";
                                }
                                
                                    
                                $rc = mysqli_stmt_close($stmt);
                            }
                        }
                    }
                    
                    $output3 = ob_get_clean();
                    
                    if ($output3 !== "")
                    {
                        echo "<tr>
                        <td>
                            $blockName ( $timeStart - $timeEnd )
                            <button onclick='hideNextRow(this.parentNode.parentNode)'>Show Bookings</button>
                        </td>
                        </tr>";
                        
                        echo "<tr class = 'bookingRow' hidden><td><table>";
                        echo "<tbody>";
                        echo $output3;
                        echo "</tbody>";
                        echo "</table></td></tr>";
                    }
                }
                
                $output2 = ob_get_clean();
                
                if ($output2 !== "")
                {

                    echo "<tr>
                        <td>
                            <span>{$dayInfo['date']} ({$day['name']})</span>
                            <button class='showButton' onclick='hideNextRow(this.parentNode.parentNode)'>Show Blocks</button>
                        </td>
                    </tr>";

                    echo "<tr hidden class='blockRow'><td><table><tbody>";
                    echo $output2;
                    echo "</tbody></table></td></tr>";
                }
            }
            
            $output1 = ob_get_clean();
            if ($output1 !== "")
            {
                echo $output1;

            }
        }
    }

    function displayUserBookings()
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
    
        if (!isset($_SESSION["userCredentials"]))
            return;
                       
        $connection = db_connect();
        
        $bookings = [];
        $stmt = mysqli_prepare($connection, "
            SELECT 
                bookings.id,
                bookings.bookingDate,
                rooms.friendlyName,
                bookings.blockId
            FROM
                bookings
            LEFT JOIN rooms ON (rooms.id = bookings.roomId)
            WHERE
                (bookings.userId = ?)
                AND (bookings.bookingDate >= ?)
                AND (rooms.active)
        ");
                        
        $today = date("Y-m-d");
        mysqli_stmt_bind_param($stmt, "is", $_SESSION['userCredentials']['id'], $today);
        mysqli_stmt_bind_result($stmt, $bookingId, $bookingDate, $roomName, $blockId);
        mysqli_stmt_execute($stmt);
        while (mysqli_stmt_fetch($stmt))
        {
            $bookings[] = [
                "id" => $bookingId,
                "date"=>$bookingDate,
                "roomName"=>$roomName,
                "blockId"=>$blockId
            ];
        }
        mysqli_stmt_close($stmt);
                        
        foreach($bookings as &$booking)
        {
            $day = getDayInfo($booking["date"], $booking["date"])[0]["day"];
            $block = $day["blocks"][$booking["blockId"]];
                            
            echo "<tr>";
            echo "<td>{$booking['roomName']}</td>";
            echo "<td>{$booking['date']}</td>";
            echo "<td>{$block['name']} </td>";
            echo "<td>{$block['timeStart']} </td>";
            echo "<td>{$block['timeEnd']} </td>";
            echo "<td><button onclick='jsCancelBooking({$booking['id']}, this)' >Cancel</button></td>";
            echo "</tr>";
        }
    }

    function displayAllBookings($room, $date)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
    
        if (!isset($_SESSION["userCredentials"]))
            return;
            
        $connection = db_connect();
        
        $dayInfo = getDayInfo($date, $date)[0]["day"];
        $stmt = null;
        
        if ($room === "allRooms")
        {
            $stmt = mysqli_prepare($connection, "
                SELECT 
                    users.realName,
                    bookings.id,
                    rooms.friendlyName,
                    bookings.blockId
                FROM
                    bookings
                    LEFT JOIN rooms
                        ON (rooms.id = bookings.roomId)
                    LEFT JOIN users
                        ON (users.id = bookings.userId)
                WHERE
                    (bookings.bookingDate = ?)
                ORDER BY 
                    bookings.id
            ");
            $rc = mysqli_stmt_bind_param($stmt, "s", $date);
        }
        else
        {
            $stmt = mysqli_prepare($connection, "
                SELECT 
                    users.realName,
                    bookings.id,
                    rooms.friendlyName,
                    bookings.blockId
                FROM
                    bookings
                    LEFT JOIN rooms
                        ON (rooms.id = bookings.roomId)
                    LEFT JOIN users
                        ON (users.id = bookings.userId)
                WHERE
                    (bookings.bookingDate = ?)
                    AND (bookings.roomId = ?)
                ORDER BY 
                    bookings.id
            ");
            $rc = mysqli_stmt_bind_param($stmt, "si", $date, $room);
        }
        
        $rc = mysqli_stmt_bind_result($stmt, $realName, $bookingId, $roomName, $blockId);
        $rc = mysqli_stmt_execute($stmt);

        while (mysqli_stmt_fetch($stmt))
        {
            $block = $dayInfo["blocks"][$blockId];
            echo "<tr>";
            echo "<td>{$realName}</td>";
            echo "<td>{$roomName}</td>";
            echo "<td>{$block['name']} - ({$block['timeStart']} - {$block['timeEnd']}) </td>";
            echo "</tr>";
        }
        
        $rc = mysqli_stmt_close($stmt);
        
    }

    function makeBooking($roomId, $blockId, $date)
    {
    
        if (session_status() == PHP_SESSION_NONE)
            session_start();
            
        if (!isset($_SESSION["userCredentials"]))
            return;
            
        $connection = db_connect();
        
        $stmt = mysqli_prepare($connection, "
            INSERT INTO bookings (roomId, userId, bookingDate, blockId)
            VALUES (?, ?, ?, ?)
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "iisi", $roomId, $_SESSION["userCredentials"]["id"], $date, $blockId);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);

    }
    
    function cancelBooking($bookingId)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
    
        if (!isset($_SESSION["userCredentials"]))
            return;
        
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            DELETE FROM bookings
            WHERE (id = ?) AND (userId = ?)
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "ii", $bookingId, $_SESSION["userCredentials"]["id"]);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
    }



?>