
INSERT INTO users (realName, email, passHash, isAdmin, active) VALUES ("System", "system@gmail.com", "$2y$10$J5lJt2Ux.dU/FGhT1XSPV./svfpm9kVKHQH2brjpggPoWCr7wVKbW", 1, 1);

INSERT INTO rooms (friendlyName, active) VALUES ("101", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("103", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("105", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("107", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("109", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("111", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("114", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("115", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("116", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("116A", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("118", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("119", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("121", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("123", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("126", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("127", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("128", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("129", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("130", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("131", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("132", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("173", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("C1", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("C2", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("C3", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("C4", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("C5", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("C6", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("C7", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("C8", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("C10", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("C12", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("C13", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("C15", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("C16", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("M1", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("M3", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("S1", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("S2", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("S3", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("S4", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("S5", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("S6", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("S7", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("S8", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("S9", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("201", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("202", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("203", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("204", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("205", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("207", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("208", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("210A", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("211", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("212", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("213", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("216", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("217", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("218", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("219", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("220", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("224", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("227", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("228", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("229", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("231", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("233", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("235", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("237", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("239", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("301", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("302", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("303", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("306", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("305", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("307", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("308", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("309", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("310", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("311", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("312", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("314", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Art Office", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Board Room", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Boys Single Gym", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Cafeteria", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Computer Studies Office", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Conference Room", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Double Gym", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("English/French Office", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Girls Single Gym", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Health Room", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("History/Geography Office", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("JETFAC", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Library Alcove", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Library General Area", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Library Lab Atwood", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Library Lab Hadfield", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Math Office", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Music Office", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Physical Education Office", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Science Office", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Social Studies/Humanities Office", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Staff Room", 1);
INSERT INTO rooms (friendlyName, active) VALUES ("Swimming Pool", 1);
INSERT INTO days (dayName) VALUES ("No school");
INSERT INTO days (dayName) VALUES ("Day 1");
INSERT INTO days (dayName) VALUES ("Day 2");
INSERT INTO days (dayName) VALUES ("Day 3");
INSERT INTO days (dayName) VALUES ("Day 4");
INSERT INTO days (dayName) VALUES ("Late Day 1");
INSERT INTO days (dayName) VALUES ("Late Day 2");
INSERT INTO days (dayName) VALUES ("Late Day 3");
INSERT INTO days (dayName) VALUES ("Late Day 4");
INSERT INTO days (dayName) VALUES ("Short Day 1");
INSERT INTO days (dayName) VALUES ("Short Day 2");
INSERT INTO days (dayName) VALUES ("Short Day 3");
INSERT INTO days (dayName) VALUES ("Short Day 4");

INSERT INTO time_blocks (blockName) VALUES
("Period A"),
("Period B"),
("Period C"),
("Period D"),
("Lunch"),
("After School");

INSERT INTO day_block_times (dayId, blockId, timeStart, timeEnd) VALUES
(2, 1, "09:00:00", "10:00:00"), -- Day 1 Block A
(2, 2, "10:30:00", "11:30:00"), -- Day 1 Block B
(2, 3, "11:30:00", "12:30:00"), -- Day 1 Lunch Block
(2, 4, "12:45:00", "13:45:00"), -- Day 1 Block C
(2, 5, "14:00:00", "15:00:00"), -- Day 1 Block D
(2, 6, "15:00:00", "17:00:00"), -- Day 1 After School
(3, 1, "09:00:00", "10:00:00"), -- Day 2 Block C
(3, 2, "10:30:00", "11:30:00"), -- Day 2 Block D
(3, 3, "11:30:00", "12:30:00"), -- Day 2 Lunch Block
(3, 4, "12:45:00", "13:45:00"), -- Day 2 Block A
(3, 5, "14:00:00", "15:00:00"), -- Day 2 Block B
(3, 6, "15:00:00", "17:00:00"), -- Day 2 After School
(4, 1, "09:00:00", "10:00:00"), -- Day 3 Block B
(4, 2, "10:30:00", "11:30:00"), -- Day 3 Block A
(4, 3, "11:30:00", "12:30:00"), -- Day 3 Lunch Block
(4, 4, "12:45:00", "13:45:00"), -- Day 3 Block D
(4, 5, "14:00:00", "15:00:00"), -- Day 3 Block C
(4, 6, "15:00:00", "17:00:00"), -- Day 3 After School
(5, 1, "09:00:00", "10:00:00"), -- Day 4 Block D
(5, 2, "10:30:00", "11:30:00"), -- Day 4 Block C
(5, 3, "11:30:00", "12:30:00"), -- Day 4 Lunch Block
(5, 4, "12:45:00", "13:45:00"), -- Day 4 Block A
(5, 5, "14:00:00", "15:00:00"), -- Day 4 Block B
(5, 6, "15:00:00", "17:00:00");  -- Day 4 After School

INSERT INTO schedule_exception_types (exceptionName) VALUES ("SkipDay");
INSERT INTO schedule_exception_types (exceptionName) VALUES ("ReplaceDay");
