<?php

    require_once("database.php");

    function displayUserList()
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
    
        $connection = db_connect();
    
        $stmt = mysqli_prepare($connection, "
            SELECT id, realName, email, isAdmin, active 
            FROM users
            ORDER BY realName
        ");
        
        $rc = mysqli_stmt_bind_result($stmt, $userId, $realName, $email, $isAdmin, $isActive);
        $rc = mysqli_stmt_execute($stmt);
        while (mysqli_stmt_fetch($stmt))
        {
            echo "<tr>";
                        
            $adminAction = ($isAdmin === 1 ? 'Revoke Admin' : 'Grant Admin');
            $statusAction = ($isActive === 1 ? 'Disable' : 'Enable');
            
            $adminBool = ($isAdmin === 1 ? false : true);
            $statusBool = ($isActive === 1 ? false : true);
            
            echo "<td>$userId</td>";
            echo "<td>$realName</td>";
            echo "<td>$email</td>";
            
            if ($userId === $_SESSION["userCredentials"]["id"])
            {
                echo "<td></td>";
            }
            else
            {
                echo "<td>
                    <form onsubmit = 'return jsSetUserAdmin($userId, $adminBool)'>
                        <input type='submit' value='$adminAction'>
                    </form>
                    <form onsubmit = 'return jsSetUserActive($userId, $statusBool)'>
                        <input type='submit' value='$statusAction'>
                    </form>
                </td>";
            }
            
            echo "</tr>";
        }
        $rc = mysqli_stmt_close($stmt); 
    
    }

    function setUserActive($userId, $active)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
            
        $connection = db_connect();
    
        $stmt = mysqli_prepare($connection, "
            UPDATE users
            SET active = ?
            WHERE id = ?
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "ii", $active, $userId);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
    }
    
    function setUserAdmin($userId, $isAdmin)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
$connection = db_connect();
    
        $stmt = mysqli_prepare($connection, "
            UPDATE users
            SET isAdmin = ?
            WHERE id = ?
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "ii", $isAdmin, $userId);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
    }
?>