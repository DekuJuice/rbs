<?php 
    require_once("database.php");

    function displayRoomList()
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
            
        $connection = db_connect();
        
        $stmt = mysqli_prepare($connection, "
            SELECT id, friendlyName, active 
            FROM rooms
            ORDER BY id DESC
        ");
                    
        $rc = mysqli_stmt_bind_result($stmt, $roomId, $friendlyName, $isActive);
        $rc = mysqli_stmt_execute($stmt);
                    
        while (mysqli_stmt_fetch($stmt))
        {
            echo "<tr class ='roomRow'>";
            echo "<td>";
            echo "<form action='editroom.php' method='POST' onsubmit='return jsDeleteRoom($roomId);' >
                    <input type='submit'name = 'deleteRoom'value='Delete'>
                    <input type='hidden'name = 'roomId' value='$roomId'>
                </form>";
                
            echo "<form action='editroom.php' method='POST' onclick='return jsSetRoomActive($roomId, this);'>" 
            . "<input type='hidden' name='roomIsActive' class = 'roomActiveValue' value='" . !$isActive . "'>"
            . "<input type='hidden' name='roomId' value='$roomId'>"
            . "<input type='submit' name='setRoomActive'value='" . ($isActive === 1 ? "Deactivate" : "Activate") . "'>"
            . "</form>";
            
            echo "</td>";
            echo "<td class='roomId'>$roomId</td>";
            echo "<td class='roomName'>
                <form action='editroom.php' method='POST' onsubmit='return jsSetRoomName($roomId, this);'>
                    <input type='text' value='$friendlyName' name='newRoomName' class='roomNameInput' onblur='setTextFieldDirty(this);'>
                    <input type='submit' name='setRoomName' value='Save'>
                </form>
            </td>";
            
            echo "</tr>";
        }
                    
        $rc = mysqli_stmt_close($stmt);
        
    }
    
    function addNewRoom($roomName)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
            
        $connection = db_connect();

        $stmt = mysqli_prepare($connection,"
            INSERT INTO rooms (friendlyName, active) VALUES (?, 1)
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "s", $roomName);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
        
    }
    
    function setRoomActive($roomId, $isActive)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
            
        $connection = db_connect();
        
        $stmt = mysqli_prepare($connection, "
            UPDATE rooms
            SET active = ?
            WHERE id = ?
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "ii", $isActive, $roomId);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
        
    }
    
    function setRoomName($roomId, $newName)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
            
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            UPDATE rooms
            SET friendlyName = ?
            WHERE id = ?
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "si", $newName, $roomId);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
        
    }
    
    function deleteRoom($roomId)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
            
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            DELETE FROM rooms
            WHERE id = ?
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "i", $roomId);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
        
    }
?>