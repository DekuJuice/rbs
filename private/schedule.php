<?php
    const EXCEPTION_SKIP_TYPE = 1;
    const EXCEPTION_REPLACE_TYPE = 2;

    require_once("database.php");

    function get_schedule(&$date, &$schedules)
    {
        $dateString = $date->format("Y-m-d");
        
        foreach ($schedules as &$schedule)
        {
            $startString = $schedule["startDate"]->format("Y-m-d");
            $endString = $schedule["endDate"]->format("Y-m-d");
            $bool = ($dateString >= $startString && $dateString <= $endString);

            // For some reason, the 2nd if statement ALWAYS executes, even when var_dump
            // says $bool is a bool(false)...
            
            // Yeah, ?????????. I blame PHP for this one.

            if ($bool === false)
            {
                //error_log("false");
                continue;
            }

            if ($bool === 1);
            {
                //error_log($dateString . " " . $startString . " " . $endString);
                //var_dump($bool);
                //error_log($dateString . " " . "???");
                return $schedule;
            }
        }
    }

    function getDayInfo($startDate, $endDate)
    {
        if (!$endDate)
            $endDate = $startDate;
     
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]))
            return;
     
        $connection = db_connect();
        
        // Get the day/block information for each day in the system, as exceptions may include
        // days not in any schedule
        
        $days = [];
        $stmt = mysqli_prepare($connection, "
            SELECT
                id, dayName 
            FROM days
            ORDER BY id 
        ");
        
        $rc = mysqli_stmt_bind_result($stmt, $dayId, $dayName);
        $rc = mysqli_stmt_execute($stmt);
        
        while (mysqli_stmt_fetch($stmt))
        {
            $days[$dayId] = [
                "name"=>$dayName,
                "blocks"=>[]
            ];
        }
        $rc = mysqli_stmt_close($stmt);
        
        // No days in system, no results
        if (count($days) === 0)
            return;
        
        // Fetch block information
        foreach ($days as $dayId => &$day)
        {
            $stmt = mysqli_prepare($connection, "
                SELECT 
                    day_block_times.timeStart, 
                    day_block_times.timeEnd,
                    time_blocks.id,
                    time_blocks.blockName
                FROM
                    time_blocks
                    LEFT JOIN day_block_times
                        ON ( day_block_times.blockId IS NULL OR time_blocks.id = day_block_times.blockId)
                        AND (day_block_times.dayId IS NULL OR day_block_times.dayId = ?)
                ORDER BY
                    day_block_times.timeStart
            ");
            $rc = mysqli_stmt_bind_param($stmt, "i", $dayId);
            $rc = mysqli_stmt_bind_result($stmt, $blockTimeStart, $blockTimeEnd, $blockId, $blockName);
            $rc = mysqli_stmt_execute($stmt);
            while (mysqli_stmt_fetch($stmt))
            {
                $day["blocks"][$blockId] = [
                    "timeStart" => $blockTimeStart,
                    "timeEnd" => $blockTimeEnd,
                    "name" => $blockName
                ];
            }
            $rc = mysqli_stmt_close($stmt);
        }
        
        // Get each schedule that intersects the specified date range
    
        // Minimum date should be the start of the schedule, to include exception 
        // dates before the specified search range
        $minDate = $startDate;
        $stmt = mysqli_prepare($connection, "
            SELECT 
                id, startDate, endDate 
            FROM schedules 
            WHERE (startDate <= ? ) and (endDate >= ?)
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "ss", $endDate, $startDate);
        $rc = mysqli_stmt_bind_result($stmt, $scheduleId, $scheduleStartDate, $scheduleEndDate);
        $rc = mysqli_stmt_execute($stmt);

        $schedules = [];

        while (mysqli_stmt_fetch($stmt))
        {   
            $schedules[] = [
                "id" => $scheduleId,
                "startDate" => new DateTime($scheduleStartDate),
                "endDate" => new DateTime( $scheduleEndDate),
                "days" => []
            ];
            $minDate = min($minDate, $scheduleStartDate);
        }
        
        mysqli_stmt_close($stmt);
        
        // No schedules for this date, no results
        if (count($schedules) === 0)
            return;
        
         // Get the ordered days for each schedule
        foreach ($schedules as &$schedule)
        {
            $stmt = mysqli_prepare($connection, "
                SELECT dayId
                FROM schedule_days
                WHERE scheduleId = ?
                ORDER BY positionInOrder
            ");
            
            $rc = mysqli_stmt_bind_param($stmt, "i", $schedule["id"]);
            $rc = mysqli_stmt_bind_result($stmt, $dayId);
            $rc = mysqli_stmt_execute($stmt);
            while (mysqli_stmt_fetch($stmt))
            {
                $schedule["days"][] = &$days[$dayId];
            }
            $rc = mysqli_stmt_close($stmt);
        }
        
        //Get any day exceptions in the date range
        $dayExceptions = [];
        $stmt = mysqli_prepare($connection, "
            SELECT 
                replacementDayId, exceptionDate, exceptionType
            FROM 
                schedule_exceptions
            WHERE (exceptionDate >= ?) AND (exceptionDate <= ?)
            ORDER BY exceptionDate

        ");
        $rc = mysqli_stmt_bind_param($stmt, "ss", $minDate, $endDate);
        $rc = mysqli_stmt_bind_result($stmt, $replacementDayId, $exceptionDate, $exceptionType);
        $rc = mysqli_stmt_execute($stmt);
        while (mysqli_stmt_fetch($stmt))
        {
            $dayExceptions[$exceptionDate] = [
                "replacementDay" => &$days[$replacementDayId],
                "type" => $exceptionType
            ];
        }
        
        $rc = mysqli_stmt_close($stmt);
        
        $results = [];
        // Iterate over each day to determine available bookings
        $interval = new DateInterval("P1D");
        $period = new DatePeriod(
            new DateTime($startDate),
            $interval, 
            (new DateTime($endDate))->modify("+1 day")
        );        

        $currentSchedule = null;
        $daysPassed = 0;
        $offsetDays = 0;

        foreach ($period as $dt)
        {
            //Exclude weekends
            if ($dt->format("N") < 6)
            {
                //Passed into a new schedule interval
                $newSchedule = get_schedule($dt, $schedules);
                                
                if ($currentSchedule != $newSchedule)
                {
                
                    $currentSchedule = $newSchedule;
                    $daysPassed = 0;
                    $offsetDays = 0;
                
                    if ($newSchedule !== null)   
                    {
                        foreach (new DatePeriod($currentSchedule["startDate"], $interval, $dt) as $dt2)
                        {
                            if ($dt2->format("N") < 6)
                                $daysPassed += 1;
                                
                            $exceptionOnDay = $dayExceptions[$dt2->format("Y-m-d")];
                            if ($exceptionOnDay && $exceptionOnDay["type"] == EXCEPTION_SKIP_TYPE)
                            {
                                $offsetDays -= 1;
                            }
                            
                        }
                    }                
                }
                
                if ($currentSchedule)
                {
                    $dayList = $currentSchedule["days"];
                    $nextDay = null;
                    $dateString = $dt->format("Y-m-d");
                    
                    if (count($dayList) > 0)
                        $nextDay = $dayList[ ($daysPassed + $offsetDays) % count($dayList)];
                    
                    if (isset($dayExceptions[$dateString]))
                    {
                        if ($dayExceptions[$dateString]["type"] == EXCEPTION_SKIP_TYPE)
                            $offsetDays += 1;
                            
                        $nextDay = $dayExceptions[$dateString]["replacementDay"];
                    }
                    
                    if ($nextDay && count($nextDay["blocks"]) > 0 )
                    {
                        $results[] = [
                            "day"=>$nextDay,
                            "date"=>$dateString
                        ];
                    }
                }
                
                $daysPassed += 1;
            }
        }
        
        return $results;
    }

    function displayDayName($date)
    {
        $info = getDayInfo($date, $date);
        if (!$info)
            return;
        
        echo "(";
        echo $info[0]["day"]["name"];
        echo ")";
    }
    
    function displayDayBlocks($date)
    {
        $info = getDayInfo($date, $date);
        
        if (!$info)
            return;
        
        $day = $info[0]["day"];
        
        echo "<tr><th>{$day['name']}</th></tr>";
        
        foreach ($day["blocks"] as &$block)
        {
            echo "<tr>
                <td>{$block['name']} - ({$block['timeStart']} - {$block['timeEnd']})</td>
            </tr>";
        }        
    }
    
    
?>