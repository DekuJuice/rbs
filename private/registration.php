<?php
    require_once("database.php");
    
    /*
     * Generate the authentication email message.
     */
    
    function renderRegistrationEmail($realName, $authenticationToken)
    {
        ob_start();
        include("registration_email_template.php");
        return ob_get_contents();
    }
    
    /*
     * Registers a new user to the system. Registered users cannot actually
     * access the system yet, they must first verify their email with the
     * authentication token that is sent to them.
     * 
     * Returns an array containing any errors that occured, such as failing
     * to connect to the database, or invalid email/passwords
     */
        
    function registerUser($newEmail, $newRealName, $newPassword, $confirmPassword)
    {
        $config = parse_ini_file(dirname(__FILE__) . "/../private/config.ini");
        $connection = db_connect();
        $registrationError = [];
        
        if (!$connection)
            $registrationError[] = "A connection error has occured. Please try again later";
            
        // User must provide an email.
        if ($newEmail == null)
            $registrationError[] = "An email must be provided.";
        else
        {
            // Only emails of the specified domain are allowed
            $newEmail .= $config["emailDomain"];
            
            // Email address must be valid.
            if (!filter_var($newEmail, FILTER_VALIDATE_EMAIL))
                $registrationError[] = "Please enter a valid email address.";
        }
        // User must provide their real name.
        if ($newRealName == null)
            $registrationError[] = "Your real name is required.";
    
        // User must enter a password.
        if ($newPassword == null)
            $registrationError[] = "A password is required.";
        // Passwords must be at least 8 characters long.
        elseif (strlen($newPassword) < 8)
            $registrationError[] = "Your password must be at least 8 characters long.";
        // Confirmation password must be the same
        elseif ($newPassword !== $confirmPassword)
            $registrationError[] = "Your passwords do not match.";
        
        
        // No errors in info provided so far, add the user to the system.
        if (count($registrationError) == 0)
        {
            // Hash and salt the password.
            $hashedPassword = password_hash( $newPassword, PASSWORD_BCRYPT);    
            
            // Hash and salt the user's email to generate an authentication token
            $authenticationToken = password_hash( $newEmail, PASSWORD_BCRYPT);
            
            $stmt = mysqli_prepare($connection, "
                INSERT INTO pending_registrations 
                    (realName, email, passHash, authenticationToken) 
                VALUES 
                    (?,?,?,?)
            ");
            
            $rc = mysqli_stmt_bind_param($stmt, "ssss", $newRealName, $newEmail, $hashedPassword, $authenticationToken);
            $rc = mysqli_stmt_execute($stmt);
            $rc = mysqli_stmt_close($stmt);

            // Send authentication email
            mail(
                $newEmail, 
                "Confirm your Room Booking System account", 
                renderRegistrationEmail($newRealName, $authenticationToken) 
            );
        }
        
        return $registrationError;        
    }
    
    /*
     * Authenticates a registered user. If successful, that user is added
     * to the "users" table proper, and access to the system is granted.
     */
    
    function authenticateUser($authenticationToken)
    {
        $connection = db_connect();
        
        $stmt = mysqli_prepare($connection, "
            SELECT 
                email, realName, passHash, authenticationToken
            FROM
                pending_registrations
            WHERE
                authenticationToken = ?
            
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "s", $authenticationToken);
        $rc = mysqli_stmt_bind_result($stmt, $email, $realName, $passHash, $storedToken);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_fetch($stmt);
        $rc = mysqli_stmt_close($stmt);
        
        if ($authenticationToken === $storedToken);
        {
            
            $stmt = mysqli_prepare($connection, "
                INSERT INTO users (email, realName, passHash, isAdmin, active) VALUES (?,?,?,0,1)
            ");
                        
            $rc = mysqli_stmt_bind_param($stmt, "sss", $email, $realName, $passHash);
            $rc = mysqli_stmt_execute($stmt);
            $rc = mysqli_stmt_close($stmt);
        
            $stmt = mysqli_prepare($connection, "
                DELETE FROM pending_registrations 
                WHERE authenticationToken = ?
            ");
            
            $rc = mysqli_stmt_bind_param($stmt, "s", $storedToken);
            $rc = mysqli_stmt_execute($stmt);
            $rc = mysqli_stmt_close($stmt);
            
            return true;
        }
        
        return false;
    }
?>