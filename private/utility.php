<?php
    function savePostValue($varName)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
            
        if (isset($_POST[$varName]))
            $_SESSION[$varName] = $_POST[$varName];
        
            
    }

    function displaySavedPostValue($varName)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        echo "\"";
        if (isset($_SESSION[$varName]))
        {
            echo "{$_SESSION[$varName]}";
            unset($_SESSION[$varName]);
        }
        echo "\"";
    }
?>