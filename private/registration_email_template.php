<?php
    
    echo "Thank you for registering to the room booking system, $realName.\n";
    echo "Follow the link below to activate your account.\n";
    echo "https://{$_SERVER['HTTP_HOST']}/1/authenticate.php?authenticationToken=$authenticationToken";

?>