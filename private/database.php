<?php 

function db_connect()
{
    static $connection;
    $pathToConfig = dirname(__FILE__) . "/../private/config.ini";
    
    if (!isset($connection))
    {
        $config = parse_ini_file($pathToConfig);
        $connection = mysqli_connect($config["host"], $config["username"], $config["password"], $config["dbname"]);
    }
    
    return $connection;
}

function db_close()
{
    $connection = db_connect();
    if (!$connection)
        return;
    
    mysqli_close($connection);
}

?>