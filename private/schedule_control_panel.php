<?php 

    require_once("database.php");

    function displayDayList()
    {
    
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
            
        $connection = db_connect();
        
        $days = [];
        
        $stmt = mysqli_prepare($connection, "
            SELECT id, dayName FROM days
            ORDER BY id
        ");
                    
        $rc = mysqli_stmt_bind_result($stmt, $dayId, $dayName);
        $rc = mysqli_stmt_execute($stmt);
        while (mysqli_stmt_fetch($stmt))
        {
            $days[] = [
                "id" => $dayId,
                "name" => $dayName
            ];
        }
        
        $rc = mysqli_stmt_close($stmt);        
                
        foreach ($days as &$day)
        {
            echo "<tr>";
            echo "<td>
                <form action='editschedule.php' method='POST' onsubmit = 'return jsDeleteDay({$day['id']})'>
                    <input type='hidden' name='dayId' value='{$day['id']}'>
                    <input type='submit' value='Delete' name='deleteDay'>
                </form>
                <button onclick='hideNextRow(this.parentNode.parentNode);'>Show Block Times</button>
            </td>";
            echo "<td>{$day['id']}</td>";
            
            echo "<td>
                <form action='editroom.php' method='POST' onsubmit='return jsSetDayName({$day['id']}, this);'>
                    <input type='text' value='{$day['name']}' name='newDayName' class='dayNameInput' onblur='setTextFieldDirty(this);'>
                    <input type='submit' name='setDayName' value='Save'>
                </form>
            </td>";
            echo "</tr>";
            
            echo "<tr hidden><td colspan='3'>";
            echo "<table>";
            
            echo "<thead><tr>
                <th>Block Name</th>
                <th>Start Time - End Time</th>
            </tr></thead>";
            
            echo "<tbody>";
            displayBlockTimes($day["id"]);
            echo "</tbody>";
            echo "</table>";
            echo "</td></tr>";
        }
    }
        
    function displayBlockTimes($dayId)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
        
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            SELECT
                time_blocks.id, 
                time_blocks.blockName,
                day_block_times.timeStart,
                day_block_times.timeEnd
            FROM time_blocks
                LEFT JOIN day_block_times ON 
                    (day_block_times.blockId = time_blocks.id OR day_block_times.blockId IS NULL)
                    AND (day_block_times.dayId = ? OR day_block_times.dayId IS NULL)
            ORDER BY time_blocks.id
        ");
            
        $rc = mysqli_stmt_bind_param($stmt, "i", $dayId);
        $rc = mysqli_stmt_bind_result($stmt, $blockId, $blockName, $timeStart, $timeEnd);
        $rc = mysqli_stmt_execute($stmt);
        while (mysqli_stmt_fetch($stmt))
        {
            //"timeStart" => $timeStart === null ? "00:00:00" : $timeStart,
            //"timeEnd" => $timeEnd === null ? "00:00:00" : $timeEnd
            
           // $timeStart = $timeStart === null ? "00:00:00" : $timeStart;
           // $timeEnd = $timeEnd === null ? "00:00:00" : $timeEnd;
            
            echo "<tr>";
            echo "<td>{$blockName}</td>";
            echo "<td>
                <form action='editschedule.php' method='POST' onsubmit='return jsSetBlockTime(this);' >
                    <input type='hidden' name='dayId' value='{$dayId}' class='dayId'>
                    <input type='hidden' name='blockId' value='{$blockId}' class='blockId'>
                    <input type='time' name='timeStart' value='{$timeStart}' step='60' class='timeStart'>
                    -
                    <input type='time' name='timeEnd' value='{$timeEnd}' step='60' class='timeEnd'>
                    <input type='submit' name='setBlockTime' value='Save'>
                </form>
            </td>";
            echo "</tr>";
        }
        $rc = mysqli_stmt_close($stmt);        
    }
        
    function displayBlockList()
    {
    
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
            
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            SELECT id, blockName 
            FROM time_blocks
            ORDER BY id
        ");
        $rc = mysqli_stmt_bind_result($stmt, $blockId, $blockName);
        $rc = mysqli_stmt_execute($stmt);
        while (mysqli_stmt_fetch($stmt))
        {
            echo "<tr>";
            echo "<td>
                <form action = 'editschedule.php' method='POST' onsubmit='return jsDeleteBlock($blockId);'>
                    <input type='hidden' name='blockId' value='$blockId'>
                    <input type='submit' value='Delete' name='deleteBlock' onclick='event.stopPropagation()'>
                </form>
            </td>";
            echo "<td>$blockId</td>";
            echo "<td>
                <form action='editroom.php' method='POST' onsubmit='return jsSetBlockName($blockId, this);'>
                    <input type='text' value='$blockName' name='newBlockName' class='blockNameInput' onblur='setTextFieldDirty(this);'>
                    <input type='submit' name='setBlockName' value='Save'>
                </form>
            </td>";
            echo "</tr>";
        }
        $rc = mysqli_stmt_close($stmt);
    }    
    
    function displayScheduleList()
    {
    
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
            
        $connection = db_connect();
        $days = [];
        $stmt = mysqli_prepare($connection, "
            SELECT id, dayName FROM days
            ORDER BY id
        ");
                    
        $rc = mysqli_stmt_bind_result($stmt, $dayId, $dayName);
        $rc = mysqli_stmt_execute($stmt);
        while (mysqli_stmt_fetch($stmt))
        {
            $days[] = [
                "id" => $dayId,
                "name" => $dayName,
            ];
        }
        
        $rc = mysqli_stmt_close($stmt);

        $schedules = [];
        
        $stmt = mysqli_prepare($connection, "
            SELECT id, startDate, endDate
            FROM schedules
            ORDER BY id
        ");
        $rc = mysqli_stmt_bind_result($stmt, $scheduleId, $startDate, $endDate);
        $rc = mysqli_stmt_execute($stmt);
        while (mysqli_stmt_fetch($stmt))
        {
            $schedules[] = [
                "id" => $scheduleId,
                "startDate" => $startDate,
                "endDate" => $endDate,
            ];
        }
        $rc = mysqli_stmt_close($stmt);
        
        foreach ($schedules as &$schedule)
        {
            echo "<tr>";
            echo "<td>
                <form action = 'editschedule.php' method='POST' onsubmit = 'return jsDeleteSchedule({$schedule['id']});'>
                    <input type='hidden' name='scheduleId' value='{$schedule['id']}'>
                    <input type='submit' value='Delete' name='deleteSchedule' onclick='event.stopPropagation()'>
                </form>
                <button onclick='hideNextRow(this.parentNode.parentNode)'>Show Days</button>
            </td>";
            echo "<td>{$schedule['id']}</td>";
            echo "<td>{$schedule['startDate']}</td>";
            echo "<td>{$schedule['endDate']}</td>";
            echo "</tr>";
            
            echo "<tr hidden><td colspan='4'>";
            echo "<table>";
            echo "<tbody>";
            displayScheduleDays($schedule["id"]);
            echo "</tbody>";
            echo "</table>";
            echo "<form action='editschedule.php' method='POST' onsubmit='return jsAddScheduleDay($scheduleId, this);'>
            <select name='scheduleDayId' class='scheduleDayId'>";
            foreach ($days as &$day)
            {
                echo "<option value={$day['id']}>{$day['name']}</option>";
            }
            
            echo "</select>
                <input type='hidden' name='scheduleId' value='{$schedule['id']}'>
                <input type='submit' value='Add Day' name='addScheduleDay'>
            </form>";
            echo "</td></tr>";
        }
    }
    
    function displayScheduleDays($scheduleId)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
            
        $connection = db_connect();
            
        $stmt = mysqli_prepare($connection, "
            SELECT
                schedule_days.id,
                schedule_days.dayId, 
                schedule_days.positionInOrder,
                days.dayName
            FROM schedule_days
                LEFT JOIN days ON schedule_days.dayId = days.id
            WHERE schedule_days.scheduleId = ?
            ORDER BY positionInOrder
        ");
            
        $rc = mysqli_stmt_bind_param($stmt, "i", $scheduleId);
        $rc = mysqli_stmt_bind_result($stmt, $scheduleDayId, $dayId, $positionInOrder, $dayName);
        $rc = mysqli_stmt_execute($stmt);
        
        
        while (mysqli_stmt_fetch($stmt))
        {
            $nextPosition = $positionInOrder + 1;
            $prevPosition = $positionInOrder - 1;
            echo "<tr>";
            echo "<td>{$dayName}</td>";
            echo "<td>
                <form action='editschedule.php' method='POST' onsubmit='return false;'>
                    <input type='hidden' name='scheduleId' value='{$scheduleId}'>
                    <input type='hidden' name='dayPosition' value='{$positionInOrder}'>
                    <input type='submit' value='Move Up' name='moveDayUp' onclick='jsSetDayOrder($scheduleId, $positionInOrder, $prevPosition, this)'>
                    <input type='submit' value='Move Down' name='moveDayDown' onclick='jsSetDayOrder($scheduleId, $positionInOrder, $nextPosition, this)'>
                    <input type='submit' value='Delete' name='deleteScheduleDay' onclick = 'jsDeleteScheduleDay($scheduleId, $positionInOrder, this)'>
                </form>
            </td>";
            echo "</tr>";
            
        }
        $rc = mysqli_stmt_close($stmt);
    }
    
    function displayExceptionList()
    {
    
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
            
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            SELECT 
                schedule_exceptions.id,
                days.dayName, 
                schedule_exception_types.exceptionName,
                schedule_exceptions.exceptionDate
            FROM schedule_exceptions
                LEFT JOIN days ON days.id = schedule_exceptions.replacementDayId
                LEFT JOIN schedule_exception_types ON schedule_exception_types.id = schedule_exceptions.exceptionType
            ORDER BY schedule_exceptions.exceptionDate
        ");
        $rc = mysqli_stmt_bind_result($stmt, $exceptionId, $dayName, $exceptionName, $exceptionDate);
        $rc = mysqli_stmt_execute($stmt);
        while (mysqli_stmt_fetch($stmt))
        {
            echo "<tr>";
            echo "<td>
                <form action = 'editschedule.php' method='POST' onsubmit = 'return jsDeleteException($exceptionId)'>
                    <input type='hidden' name='exceptionId' value='$exceptionId'>
                    <input type='submit' value='Delete' name='deleteException'>
                </form>
            </td>";
            echo "<td>$exceptionId</td>";
            echo "<td>$exceptionDate</td>";
            echo "<td>$dayName</td>";
            echo "<td>$exceptionName</td>";
            echo "</tr>";
        }
        $rc = mysqli_stmt_close($stmt);
    }
    
    function addNewDay($dayName)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
        
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            INSERT INTO days (dayName) VALUES (?)
        ");
        $rc = mysqli_stmt_bind_param($stmt, "s", $dayName);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
    }
    
    function deleteDay($dayId)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
    
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            DELETE FROM days WHERE id = ?
        ");
        $rc = mysqli_stmt_bind_param($stmt, "i", $dayId);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
    }
    
    function setDayName($dayId, $dayName)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
            
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            UPDATE days
            SET dayName = ?
            WHERE id = ?
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "si", $dayName, $dayId);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
    }
    
    function setBlockName($blockId, $blockName)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
            return;
            
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            UPDATE time_blocks
            SET blockName = ?
            WHERE id = ?
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "si", $blockName, $blockId);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
    }
    
    function setBlockTime($dayId, $blockId, $timeStart, $timeEnd)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]))
            return;
                
        $connection = db_connect();

        $stmt = mysqli_prepare($connection, "
            SELECT EXISTS( SELECT 1 FROM day_block_times WHERE (blockId = ? AND dayId = ?))
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "ii", $blockId, $dayId); 
        $rc = mysqli_stmt_bind_result($stmt, $exists);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_fetch($stmt);
        $rc = mysqli_stmt_close($stmt);
        
        if ($exists && ($timeStart == null || $timeEnd == null))
        {
            $stmt = mysqli_prepare($connection, "
                DELETE FROM day_block_times
                WHERE (dayId = ? AND blockId = ?)
            ");
            $rc = mysqli_stmt_bind_param($stmt, "ii", $dayId, $blockId);
            $rc = mysqli_stmt_execute($stmt);
            $rc = mysqli_stmt_close($stmt);
        }
        elseif ($exists)
        {
            $stmt = mysqli_prepare($connection, "
                UPDATE day_block_times
                SET timeStart = ?, timeEnd = ?
                WHERE (blockId = ? AND dayId = ?)
            ");
            
            $rc = mysqli_stmt_bind_param($stmt, "ssii", $timeStart, $timeEnd, $blockId, $dayId);
            $rc = mysqli_stmt_execute($stmt);
            $rc = mysqli_stmt_close($stmt);
        }
        else
        {
            $stmt = mysqli_prepare($connection, "
                INSERT INTO day_block_times (blockId, dayId, timeStart, timeEnd)
                VALUES (?, ?, ?, ?)
            ");
            $rc = mysqli_stmt_bind_param($stmt, "iiss", $blockId, $dayId, $timeStart, $timeEnd);
            $rc = mysqli_stmt_execute($stmt);
            $rc = mysqli_stmt_close($stmt);
        }
    }
    
    function addNewBlock($blockName)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]))
            return;
            
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            INSERT INTO time_blocks (blockName) VALUES (?)
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "s", $blockName);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
    }
    
    function deleteBlock($blockId)
    {        
    
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]))
            return;
        
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            DELETE FROM time_blocks WHERE id = ?
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "i", $blockId);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
    }
    
    function addSchedule($startDate, $endDate)
    {
        
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]))
            return;
    
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            INSERT INTO schedules (startDate, endDate) VALUES (?, ?)
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "ss", $startDate, $endDate);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
    }

    function deleteSchedule($scheduleId)
    {
    
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]))
            return;
    
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            DELETE FROM schedules WHERE id = ?
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "i", $scheduleId);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
    }
    
    function addException($exceptionDate, $exceptionTypeId, $replacementDay)
    {
    
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]))
            return;
    
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            INSERT INTO schedule_exceptions (exceptionDate, exceptionType, replacementDayId ) VALUES (?, ?, ?)
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "sii", $exceptionDate, $exceptionTypeId, $replacementDay);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
    }

    function deleteException($exceptionId)
    {
    
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]))
            return;
    
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            DELETE FROM schedule_exceptions WHERE id = ?
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "i", $exceptionId);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
    }
    
    function setDayOrder($scheduleId, $oldPosition, $newPosition)
    {
    
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]))
            return;
    
        if ($oldPosition < 1 || $newPosition < 1)
            return;
            
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            SELECT COUNT(id) FROM schedule_days
            WHERE scheduleId = ?
        ");
        $rc = mysqli_stmt_bind_param($stmt, "i", $scheduleId);
        $rc = mysqli_stmt_bind_result($stmt, $maxPosition);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_fetch($stmt);
        $rc = mysqli_stmt_close($stmt);
        
        if ($oldPosition > $maxPosition || $newPosition > $maxPosition)
            return;
        
        $stmt = mysqli_prepare($connection, "
            UPDATE schedule_days
            SET 
                positionInOrder = 
                    CASE
                        WHEN positionInOrder = ? THEN ?
                        WHEN positionInOrder = ? THEN ? 
                        ELSE positionInOrder
                    END
            WHERE scheduleId = ?
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "iiiii", $oldPosition, $newPosition, $newPosition, $oldPosition, $scheduleId);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
    }
    
    function addScheduleDay($scheduleId, $dayId)
    {
    
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]))
            return;
    
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            INSERT INTO schedule_days (scheduleId, dayId, positionInOrder)
            SELECT ?, ?, MAX(positionInOrder) + 1
                FROM schedule_days
            WHERE scheduleId = ?;
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "iii", $scheduleId, $dayId, $scheduleId);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
    }
    
    function deleteScheduleDay($scheduleId, $positionInOrder)
    {
    
        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
        if (!isset($_SESSION["userCredentials"]))
            return;
    
        $connection = db_connect();
        $stmt = mysqli_prepare($connection, "
            DELETE FROM schedule_days 
            WHERE (scheduleId = ? AND positionInOrder = ?)
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "ii", $scheduleId, $positionInOrder);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);
        
        $stmt = mysqli_prepare($connection, "
            UPDATE schedule_days
            SET positionInOrder = positionInOrder - 1
            WHERE (scheduleId = ? AND positionInOrder > ?)
        ");
        
        $rc = mysqli_stmt_bind_param($stmt, "ii", $scheduleId, $positionInOrder);
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_close($stmt);

    }    

?>