<?php    
    require_once("database.php");

    function login($email, $password)
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();

        $config = parse_ini_file(dirname(__FILE__) . "/../private/config.ini");
        $connection = db_connect();
            
        $loginError = [];
        // Append the tdsb.on.ca domain to the email
        $loginEmail = $email . $config["emailDomain"];
        
        $stmt = mysqli_prepare($connection, "SELECT passHash, realName, id, isAdmin, active FROM users WHERE email = ?");
        $rc = mysqli_stmt_bind_param($stmt, "s", $loginEmail);
        $rc = mysqli_stmt_bind_result($stmt, $passHash, $realName, $id, $isAdmin, $active);
        
        $rc = mysqli_stmt_execute($stmt);
        $rc = mysqli_stmt_fetch($stmt);            
        $rc = mysqli_stmt_close($stmt);
        
        if (password_verify($password, $passHash))
        {
            if (!$active)
            {
                $loginError[] = "Your account has been disabled by an administrator. Please contact an administrator for more details.";
            }
            else
                {
                $_SESSION["userCredentials"] = [
                    "id"=>$id,
                    "email"=>$loginEmail,
                    "realName"=>$realName,
                    "isAdmin"=>$isAdmin === 1 ? true : false
                ];    
            }
        }
        else
            $loginError[] = "Your email or password is incorrect.";
    
        return $loginError;
    }
?>