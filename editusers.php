<?php
    session_start();
    
    if (!isset($_SESSION["userCredentials"]) || !$_SESSION["userCredentials"]["isAdmin"])
    {
        header("Location: index.php");
        exit;
    }
    
    require_once("private/database.php");
    require_once("private/user_control_panel.php");
?>

<!doctype HTML>
<html lang = "en">
    
    <head>
        <title>Edit Users</title>
        <link rel = "stylesheet" type = "text/css" href= "style/responsivegrid/col.css">
        <link rel = "stylesheet" type = "text/css" href= "style/responsivegrid/4cols.css">
        <link rel = "stylesheet" type = "text/css" href= "style/global.css">
        <link rel = "stylesheet" type = "text/css" href= "style/editusers.css">
        <meta charset = "utf-8">
    </head>

    <body> 
        <div class="section group">
            <div class= "col span_4_of_4">
                <nav>
                    <a href = "php/logout.php">Log Out</a>    
                    <a href="home.php">Back</a>
                </nav>
            </div>
        </div>
        
        <div class="section group">
            <div class="col span_1_of_4"></div>
            <div class="col span_2_of_4">
            
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    
                    <tbody id="userList">
                        <?php
                            displayUserList();
                        ?>
                    </tbody>
                </table>
                
            </div>
            <div class="col span_1_of_4"></div>
        </div>
        
        <script src="js/editusers.js"></script>
    </body>

</html>