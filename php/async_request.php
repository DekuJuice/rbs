<?php

    // To future maintainers: 
    // I'm sorry

    require_once("../private/room_control_panel.php");
    require_once("../private/schedule_control_panel.php");
    require_once("../private/user_control_panel.php");
    require_once("../private/bookings.php");
    
    $action = $_POST["action"];
    error_log($action);
    
    switch ($action)
    {
        case "addNewRoom":
            addNewRoom($_POST["roomName"]);
            break;
        case "deleteRoom":
            deleteRoom($_POST["roomId"]);
            break;
        case "setRoomName":
            setRoomName($_POST["roomId"], $_POST["roomName"]);
            break;
        case "setRoomActive":
            setRoomActive($_POST["roomId"], $_POST["roomIsActive"] === "1" ? true : false);
            break;
        case "displayRoomList":
            displayRoomList();
            break;
        case "displayDayList":
            displayDayList();
            break;
        case "displayBlockTimes":
            displayBlockTimes($_POST["dayId"]);
            break;
        case "displayBlockList":
            displayBlockList();
            break;
        case "displayScheduleDays":
            displayScheduleDays($_POST["scheduleId"]);
            break;
        case "displayScheduleList":
            displayScheduleList();
            break;
        case "displayExceptionList":
            displayExceptionList();
            break;
        case "displayAllBookings":
            displayAllBookings($_POST["room"], $_POST["date"]);
            break;
        case "addNewDay":
            addNewDay($_POST["dayName"]);
            break;
        case "deleteDay":
            deleteDay($_POST["dayId"]);
            break;
        case "addNewBlock":
            addNewBlock($_POST["blockName"]);
            break;
        case "deleteBlock":
            deleteBlock($_POST["blockId"]);
            break;
        case "setBlockTime":
            setBlockTime($_POST["dayId"], $_POST["blockId"], $_POST["timeStart"], $_POST["timeEnd"]);
            break;
        case "addNewSchedule":
            addSchedule($_POST["startDate"], $_POST["endDate"]);
            break;
        case "deleteSchedule":
            deleteSchedule($_POST["scheduleId"]);
            break;
        case "setDayOrder":
            setDayOrder($_POST["scheduleId"], $_POST["oldDayPosition"], $_POST["newDayPosition"]);
            break;
        case "addScheduleDay":
            addScheduleDay($_POST["scheduleId"], $_POST["dayId"]);
            break;
        case "deleteScheduleDay":
            deleteScheduleDay($_POST["scheduleId"], $_POST["positionInOrder"]);
            break;
        case "addException":
            addException($_POST["exceptionDate"], $_POST["exceptionType"], $_POST["replacementDay"]);
            break;
        case "deleteException":        
            deleteException($_POST["exceptionId"]);
            break;
        case "searchBookings":
            $param = json_decode($_POST["param"], true);
            searchBookings($param["dateStart"], $param["dateEnd"], $param["rooms"], $param["blocks"]);
            break;
        case "displayUserBookings":
            displayUserBookings();
            break;
        case "makeBooking":
            makeBooking($_POST["roomId"], $_POST["blockId"], $_POST["bookingDate"]);
            break;
        case "cancelBooking":        
            cancelBooking($_POST["bookingId"]);
            break;
        case "setUserAdmin":
            setUserAdmin($_POST["userId"], $_POST["isAdmin"]);
            break;
        case "setUserActive":
            setUserActive($_POST["userId"], $_POST["active"]);
            break;
        case "displayUserList":
            displayUserList();
            break;
        case "setDayName":
            setDayName($_POST["dayId"], $_POST["dayName"]);
            break;
        case "setBlockName":
            setBlockName($_POST["blockId"], $_POST["blockName"]);
            break;
        case "displayDayName":
            displayDayName($_POST["date"]);
            break;
        case "displayDayBlocks":
            displayDayBlocks($_POST["date"]);
            break;
        default: 
            error_log("Invalid Action $action");
    }
    db_close();
?>